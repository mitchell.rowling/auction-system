import * as supertest from 'supertest';

const request = supertest('http://api:8080');

const newUser = {
  email: 'dave.smith@gmail.com',
  firstName: 'Dave',
  lastName: 'Smith',
  age: 30,
};
let token;
let createdUserId;

describe('/v1/users', async () => {
  beforeAll(async () => {
    const response = await request.post('/v1/login').send({
      email: 'mitchell.rowling@gmail.com',
    });
    token = response.body.token;
  });

  describe('/', () => {
    test('is a protected route', async () => {
      const response = await request.get('/v1/users');
      expect(response.status).toBe(401);
    });
    test('GET /: returns an array of users', async () => {
      const response = await request
        .get('/v1/users')
        .set('Authorization', `Bearer ${token}`);
      expect(response.status).toBe(200);
      expect(response.body.length).toBe(1);
      createdUserId = response.body.id;
    });
    describe('POST /: creates a new user', async () => {
      const response = await request
        .post('/v1/users')
        .set('Authorization', `Bearer ${token}`)
        .send(newUser);
      test('returns 201', async () => {
        expect(response.status).toBe(201);
      });
      test('createdBy property is equal to current user', async () => {
        expect(response.body.createdBy).toBe(1);
      });
      test('updatedBy property is equal to current user', async () => {
        expect(response.body.updatedBy).toBe(1);
      });
    });
    describe('POST /: throws an error if duplicate email', async () => {
      const response = await request
        .post('/v1/users')
        .set('Authorization', `Bearer ${token}`)
        .send(newUser);
      test('returns 500', async () => {
        expect(response.status).toBe(500);
      });
    });
    describe('GET /id: returns a user by id', async () => {
      const userResponse = await request
        .get(`/v1/users/${createdUserId}`)
        .set('Authorization', `Bearer ${token}`);
      test('returns 200', async () => {
        expect(userResponse.status).toBe(200);
      });
      test('createdBy property is equal to current user', async () => {
        expect(userResponse.body.createdBy).toBe(1);
      });
      test('updatedBy property is equal to current user', async () => {
        expect(userResponse.body.updatedBy).toBe(1);
      });
      test('email property is equal to previously created used', async () => {
        expect(userResponse.body.email).toBe(newUser.email);
      });
    });
    describe('PATCH: updates an existing user', async () => {
      const newAge = 31;
      const response = await request
        .patch(`/v1/users/${createdUserId}`)
        .set('Authorization', `Bearer ${token}`)
        .send({
          age: newAge,
        });
      test('returns 204', () => {
        expect(response.status).toBe(204);
      });
      test('age has been updated', async () => {
        const updatedUser = await request
          .get(`/v1/users/${createdUserId}`)
          .set('Authorization', `Bearer ${token}`);
        expect(updatedUser.body.age).toBe(newAge);
      });
    });
  });
});

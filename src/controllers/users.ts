import { User } from '../entity/User';

import { create, deleteById, getAll, patchById, readById } from './core';

export const usersGetAll = getAll(User);
export const usersCreate = create(User);
export const usersFindById = readById(User);
export const usersPatchById = patchById(User);
export const usersDeleteById = deleteById(User);
